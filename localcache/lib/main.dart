import 'package:flutter/material.dart';
import 'package:firebase/firebase.dart' as fb;
import 'package:localcache/local_cache.dart';
import 'package:localcache/local_cache_example.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LocalCacheExample(LocalCache()),
    );
  }
}
