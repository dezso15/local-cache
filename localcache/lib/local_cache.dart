import 'package:firebase/firestore.dart' as fb;
import 'package:firebase/firebase.dart' as db;
import 'package:localcache/cache_response.dart';

const DEFAULT_CACHE_DURATION = 25;

class LocalCache {
  final int cacheDuration;
  static Map<String, CacheResponse> cache = new Map<String, CacheResponse>();

  LocalCache({
    this.cacheDuration = DEFAULT_CACHE_DURATION,
  });

  Future<fb.QuerySnapshot> collectionCacheWrap(String path, {String additionalInfo, bool isForced}) async {
    String key = path + (additionalInfo ?? "");

    if (isForced ?? false) {
      final res = await db.firestore().collection(key).get();
      cache[key] = CacheResponse(timestamp: DateTime.now(), response: res);
      return Future.value(cache[key].response);
    }

    if (cache.containsKey(key)) {
      if (Duration(seconds: cacheDuration) > DateTime.now().difference(cache[key].timestamp)) {
        return Future.value(cache[key].response);
      } else {
        final res = await db.firestore().collection(key).get();
        cache[key] = CacheResponse(timestamp: DateTime.now(), response: res);
        return Future.value(cache[key].response);
      }
    } else {
      final res = await db.firestore().collection(key).get();
      cache[key] = CacheResponse(timestamp: DateTime.now(), response: res);
      return Future.value(cache[key].response);
    }
  }

  Future<fb.DocumentSnapshot> documentCacheWrap(String path, {String additionalInfo, bool isForced}) async {
    String key = path + (additionalInfo ?? "");

    if (isForced ?? false) {
      final res = await db.firestore().doc(key).get();
      cache[key] = CacheResponse(timestamp: DateTime.now(), response: res);
      return Future.value(cache[key].response);
    }

    if (cache.containsKey(key)) {
      if (Duration(seconds: cacheDuration) > DateTime.now().difference(cache[key].timestamp)) {
        return Future.value(cache[key].response);
      } else {
        final res = await db.firestore().doc(key).get();
        cache[key] = CacheResponse(timestamp: DateTime.now(), response: res);
        return Future.value(cache[key].response);
      }
    } else {
      final res = await db.firestore().doc(key).get();
      cache[key] = CacheResponse(timestamp: DateTime.now(), response: res);
      return Future.value(cache[key].response);
    }
  }

  Future<fb.QuerySnapshot> queryCacheWrap(String path, String field, String relation, dynamic value,
      {String additionalInfo, bool isForced}) async {
    String key = path + (additionalInfo ?? "") + field + relation + value.toString();
    if (isForced ?? false) {
      final res = await db.firestore().collection(path).where(field, relation, value).get();
      cache[key] = CacheResponse(timestamp: DateTime.now(), response: res);
      return Future.value(cache[key].response);
    }

    if (cache.containsKey(key)) {
      if (DateTime.now().difference(cache[key].timestamp) < Duration(seconds: cacheDuration)) {
        return Future.value(cache[key].response);
      } else {
        final res = await db.firestore().collection(path).where(field, relation, value).get();
        cache[key] = CacheResponse(timestamp: DateTime.now(), response: res);
        return Future.value(cache[key].response);
      }
    } else {
      final res = await db.firestore().collection(path).where(field, relation, value).get();
      cache[key] = CacheResponse(timestamp: DateTime.now(), response: res);
      return Future.value(cache[key].response);
    }
  }
}
