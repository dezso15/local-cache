import 'package:flutter/material.dart';
import 'package:localcache/local_cache.dart';
import 'package:firebase/firestore.dart' as fb;

class LocalCacheExample extends StatefulWidget {
  final LocalCache cache;

  LocalCacheExample(this.cache);

  @override
  _LocalCacheExampleState createState() => _LocalCacheExampleState();
}

class _LocalCacheExampleState extends State<LocalCacheExample> {
  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration.zero, () {
      setState(() {});
    });

    return Scaffold(
      body: FutureBuilder<fb.DocumentSnapshot>(
        future: widget.cache.documentCacheWrap("example/1"),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
              child: Text(snapshot.data.get("text")),
            );
          }
          return Container(
            child: Text("NO DATA"),
          );
        },
      ),
    );
  }
}
