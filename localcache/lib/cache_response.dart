class CacheResponse {
  final DateTime timestamp;
  final dynamic response;

  CacheResponse({this.response, this.timestamp});
}
